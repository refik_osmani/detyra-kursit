# DETYRA KURSIT
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title>Solution Responsive Web Template</title>
<!--

Template 2081 Solution

http://www.tooplate.com/view/2081-solution

-->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="">
		<meta name="Shit-blerje onnline" content="">

		<!-- animate -->
		<link rel="stylesheet" href="css/animate.min.css">
		<!-- bootstrap -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<!-- font-awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<!-- google font -->
		<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700,800' rel='stylesheet' type='text/css'>
		<!-- custom -->
		<link rel="stylesheet" href="css/style.css">

	</head>
	<body data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

		<!-- start navigation -->
		<div class="navbar navbar-fixed-top navbar-default" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
						<span class="icon icon-bar"></span>
					</button>
					<a href="#" class="navbar-brand"><img src="images/logos.png" class="img-responsive" alt="logos"></a>
				</div>
				<div class="collapse navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#home" class="smoothScroll">HOME</a></li>
						<li><a href="#service" class="smoothScroll">SERVICE</a></li>
						<li><a href="#about" class="smoothScroll">ABOUT</a></li>
						<li><a href="#team" class="smoothScroll">TEAM</a></li>
						<li><a href="#pricing" class="smoothScroll">PRICING</a></li>
						<li><a href="#portfolio" class="smoothScroll">PORTFOLIO</a></li>
						<li><a href="#contact" class="smoothScroll">CONTACT</a></li>
					</ul>
				</div>
			</div>
		</div>
		<!-- end navigation -->

		<!-- start home -->
		<section id="home" class="text-center">
		  <div class="templatemo_headerimage">
		    <div class="flexslider">
		      <ul class="slides">
		        <li>
		        	<img src="images/slider/12.jpg" alt="Slide 1">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1 class="wow fadeInDown" data-wow-delay="2000">Shit-Blerje Makinash</h1>
					      <h2 class="wow fadeInDown" data-wow-delay="2000">
							<span>Zgjedhja në dorën tuaj</span>
						</h2>
						<a href="#service" class="smoothScroll btn btn-default wow fadeInDown" data-wow-delay="2000">Vazhdoni më poshtë</a>	
					    </div>
				  	</div>
		        </li>
		        <li>
		        	<img src="images/slider/2.jpg" alt="Slide 2">
		        	<div class="slider-caption">
					    <div class="templatemo_homewrapper">
					      <h1 class="wow fadeInDown" data-wow-delay="2000">AUDI</h1>
					      <h2 class="wow fadeInDown" data-wow-delay="2000">
							<span>Vjen në treg me super risitë e saj.</span>
						</h2>
						<p><b>Per te perfituar ofertat tona me te lira ne treg dhet te nagvingoni ne faqen tone ku mund te beheni pjese e e nje shorteu.</b></p>
						<a href="#about" class="smoothScroll btn btn-default wow fadeInDown" data-wow-delay="2000">Konkurimi ne treg</a>	
					    </div>
				  	</div>
		        </li>
		      </ul>
		    </div>
		  </div>				
		</section>
		<!-- end home -->

		<!-- start service -->
		<div id="service">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.1s">
								<i class="fa fa-laptop"></i>
							</div>
							<div class="media-body wow fadeIn">
								<h3 class="media-heading">Shikueshmëria</h3>
								<p>Eshte nje faqe e cila u vjen ne ndihme personave te cilet jane te interesuar per blerjen e mjeteve online dhe ka per qellim te informoje klientin ne lidhje me perditesimin e fotove te mjeteve qe ato jane te interesuar.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.3s">
								<i class="fa fa-cog"></i>
							</div>
							<div class="media-body wow fadeIn">
								<h3 class="media-heading">Rregullat</h3>
								<p>Rregullat mbi shit-blerjen e makines do te zbatohen nga administruesit kryesore te kompanise te cilet jane te autorizuar ne kete per te mirmbajtur dhe per te sigurine ndaj te dhenave ndaj klientit website.</p> 
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.6s">
								<i class="fa fa-paper-plane"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.3s">
								<h3 class="media-heading">Efikasiteti</h3>
								<p>Shpejtesia dhe korretesia do jene nje nder me kryesoret e kesaj faqe per te dhene cdo shjegim te mundshem ne kohe reale .Efiçenca ne faqen tone eshte nje tjeter avantazh e cila na ben te jemi nje nder faqet me te suskseshme ne tregun boteror  e makinave te Mercedes dhe Audi.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.9s">
								<i class="fa fa-html5"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.6s">
								<h3 class="media-heading">Faqja zyrtare</h3>
								<p>Eshte nje nder faqet e autorizuara ne rrjet dhe te licensuara e cila ben te mundur konkurrencen ne treg ndaj kompanive te tjera si dhe perben shikueshmerine me te madhe ne klasifikim me faqet e tjera zyrtare.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.4s">
								<i class="fa fa-comments-o"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.3s">
								<h3 class="media-heading">Suporti</h3>
								<p>Kompania jone u vjen ne ndihme personave ne pergjigjet e tyre ne komente si dhe ne mesazhet te cilat ato kane nevoje te informohen rreth mjeteve te cilet interesohen.Per cdo pyetje dhe pergjigje admini i faqes ka per qellimi kthimin e pergjigjes ne kohe reale dhe me nje informacion te detajuar.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="media">
							<div class="media-object media-left wow fadeIn" data-wow-delay="0.8s">
								<i class="fa fa-check-circle"></i>
							</div>
							<div class="media-body wow fadeIn" data-wow-delay="0.6s">
								<h3 class="media-heading">Përditësimi</h3>
								<p>Ne lidhje me perditesimin e faqes sone ne rrjet do te meren administruesit kryesore te cilet do te meren me hyrjen e nje makine te re ne faqe apo me shitjen e ndonje makine te kerkuar nga te interesuarit.Çdo e dhene e mjetit do te jete e sakte njekohesisht dhe pergjigjia do behet ne kohe reale.Shume shpejt kjo faqe do behet qe te shkarkohet neper celularet tuaj si app store Iphone po ashtu dhe ne sistemin Android Plaz Store</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end service -->

		<!-- start divider -->
		<div id="divider">
			<div class="container">
				<div class="row">
					<div class="col-md-1 col-sm-1"></div>
					<div class="col-md-8 col-sm-8">
						<h2 class="wow bounce">Rreth <strong> Kompanisë</strong></h2>
						<h3 class="wow fadeIn" data-wow-delay="0.6s"><mark> Siguri  </mark> &amp;<mark> Cilësi </mark></h3>
						<p class="wow fadeInUp" data-wow-delay="0.9s">Kjo kompani është një nder kompanitë e mëdha në Shqiperi për shitje online.Në kompaninë tonë do të gjeni makinat më të lira në treg si nga cilesia ashtu dhe nga siguria.Ofertat tona janë të limituara prandaj duhet të nxitoni sa më parë për te blerë makinen tuaj me një cmim tepër të arseyeshëm.Nxitoni,ne ju mirëpresim.!</p>
					</div>
					<div class="col-md-2 col-sm-2"></div>
				</div>
			</div>
		</div>
		<!-- end divider -->

		<!-- start about -->
		<div id="about">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 wow fadeInLeft" data-wow-delay="0.9s">
						<h3>Konkurenca</h3>
						<h4>Siguri dhe korrektesi</h4>
						<p>Siguria eshte nje nga perparesite kryesore te dhenave te klientit dhe administratorit.</p>
						<p>Korrektesia eshte qe te jene te dy palet dakord me pagesen cash online ose pay pal ne numer llogarie 10021558897</p>
					</div>
					<div class="col-md-6 col-sm-6 wow fadeInRight" data-wow-delay="0.9s">
						<span class="text-top">Siguria<small>100%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
							</div>
						<span>Kontrata<small>80%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"></div>
							</div>
						<span>Komunikimi<small>90%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
							</div>
						<span>Publikimi<small>100%</small></span>
							<div class="progress">
								<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
							</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end about -->

		<!-- start team -->
		<div id="team">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="wow bounce">Administratori i Faqes </h2>
					
       					 <div class="col-md-4 col-sm-4 wow fadeIn" data-wow-delay="0.6s">
						<h4>Administrator/Kreator</h4>
						<h3>Refik Osmani</h3>
						<p>Administrator i website se shitblerjes online si dhe kreator i faqes.</p>
						 <ul class="social-icon text-center">
           					<li><a href="#" class="wow fadeInUp fa fa-facebook" data-wow-delay="2s"></a></li>
          					 <li><a href="#" class="wow fadeInDown fa fa-twitter" data-wow-delay="2s"></a></li>
          					 <li><a href="#" class="wow fadeIn fa fa-instagram" data-wow-delay="2s"></a></li>
         					 <li><a href="#" class="wow fadeInUp fa fa-pinterest" data-wow-delay="2s"></a></li>
       					 </ul>
					</div>
			
				</div>
			</div>
		</div>
		<!-- end team -->

		<!-- start newsletter -->
		<div id="newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title">
							<h2 class="wow bounce">Rregjistrim Kontrate</h2>
							<p class="wow fadeIn" data-wow-delay="0.6s">Rrgjistroni email-in tuaj per te vazhduar lidhjen e kontrates.</p>
						</div>
						<form action="#" method="get" class="wow fadeInUp" data-wow-delay="0.9s">
							<div class="col-md-3 col-sm-3"></div>
							<div class="col-md-4 col-sm-4">
								<input name="email" type="email" class="form-control" id="email" placeholder="Enter your email">
							</div>
							<div class="col-md-2 col-sm-2">
								<input name="submit" type="submit" class="form-control" id="submit" value="Sign Up">
							</div>
							<div class="col-md-3 col-sm-3"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- end newsletter -->

		<!-- start pricing -->
		<div id="pricing" class="text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-12 wow bounce">
						<h2>Mundesi pagimi me keste mujore.</h2>
					</div>
					<div class="col-sm-6 col-md-3 wow fadeInLeft" data-wow-delay="0.6s">
						<div class="plan plan_one">
							<h4 class="plan_title">Basic</h4>
							<ul>
								<li>$20 per month</li>
								<li>100GB Storage</li>
								<li>1,000GB Transfer</li>
								<li>10 Bootstrap Themes</li>
								<li>24-hr support</li>
							</ul>
							<button class="btn btn-warning">SIGN UP</button>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 wow fadeInUp" data-wow-delay="0.9s">
						<div class="plan plan_two">
							<h4 class="plan_title">Standard</h4>
							<ul>
								<li>$40 per month</li>
								<li>300GB Storage</li>
								<li>3,000GB Transfer</li>
								<li>30 Bootstrap Themes</li>
								<li>12-hr response</li>
							</ul>
							<button class="btn btn-warning">SIGN UP</button>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 wow fadeInDown" data-wow-delay="1s">
						<div class="plan plan_three">
							<h4 class="plan_title">Professional</h4>
							<ul>
								<li>$60 per month</li>
								<li>600GB Storage</li>
								<li>6,000GB Transfer</li>
								<li>60 Premium Themes</li>
								<li>1-hr response</li>
							</ul>
							<button class="btn btn-warning">SIGN UP</button>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay="1.3s">
						<div class="plan plan_four">
							<h4 class="plan_title">Advanced</h4>
							<ul>
								<li>$80 per month</li>
								<li>1,000GB Storage</li>
								<li>10TB Premium</li>
								<li>80 Premium Themes</li>
								<li>15-min response</li>
							</ul>
							<button class="btn btn-warning">SIGN UP</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end pricing -->

		<!-- start portfolio -->
		<div id="portfolio">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 class="wow bounce">Foto te makinave</h2>
							<div class="iso-section wow fadeIn" data-wow-delay="0.6s">

								<ul class="filter-wrapper clearfix">
                   					 <li><a href="#" data-filter="*" class="selected opc-main-bg">Te gjitha</a></li>
                   					 <li><a href="#" class="opc-main-bg" data-filter=".graphic">Perzgjedhja</a></li>
                   					 <li><a href="#" class="opc-main-bg" data-filter=".photoshop">Luksoziteti</a></li>
                    				<li><a href="#" class="opc-main-bg" data-filter=".wallpaper">Ndarja</a></li>
               					 </ul>

               				 	<div class="iso-box-section">
               				 		<div class="iso-box-wrapper col4-iso-box">

               				 			<div class="iso-box graphic photoshop wallpaper col-md-4 col-sm-6 col-xs-12">
               				 				<div class="portfolio-thumb">
               				 					<img src="images/gla1.jpg" class="fluid-img" alt="gla1 img">
               				 						<div class="portfolio-overlay">
               				 							<a href="#" class="fa fa-search"></a>
               				 							<a href="#" class="fa fa-link"></a>
               				 							<h4>Mercedes Benz GLC 75.000 Euro</h4>
               				 							<p>Shitet Mercedes Benz GLC,Viti 2017,Euro 5,Full Option</p>
               				 						</div>
               				 				</div>
               				 			</div>

               				 			<div class="iso-box graphic wallpaper col-md-4 col-sm-6 col-xs-12">
               				 				<div class="portfolio-thumb">
               				 					<img src="images/glc2.jpg" class="fluid-img" alt="glc2 img">
               				 						<div class="portfolio-overlay">
               				 							<a href="#" class="fa fa-search"></a>
               				 							<a href="#" class="fa fa-link"></a>
               				 							<h4>Mercedes Benz GLA 40,000 Euro</h4>
               				 							<p>Shitet Mercedes Benz GLA,Viti 2018,Euro 6,Full Option</p>
               				 						</div>
               				 				</div>
               				 			</div>

               				 			<div class="iso-box wallpaper col-md-4 col-sm-6 col-xs-12">
               				 				<div class="portfolio-thumb">
               				 					<img src="images/cls.jpg" class="fluid-img" alt="cls img">
               				 						<div class="portfolio-overlay">
               				 							<a href="#" class="fa fa-search"></a>
               				 							<a href="#" class="fa fa-link"></a>
               				 							<h4>Mercedes Benz CLS 75.000 Euro</h4>
               				 							<p>Mercedes Benz CLS,Viti 2018,Euro 6,Full Option</p>
               				 						</div>
               				 				</div>
               				 			</div>

               				 			<div class="iso-box graphic col-md-4 col-sm-6 col-xs-12">
               				 				<div class="portfolio-thumb">
               				 					<img src="images/aud4.jpg" class="fluid-img" alt="aud4 img">
               				 						<div class="portfolio-overlay">
               				 							<a href="#" class="fa fa-search"></a>
               				 							<a href="#" class="fa fa-link"></a>
               				 							<h4>Audi A4 35.000 Euro</h4>
               				 							<p>Audi A4,Viti 2015,Euro 6,Full Option</p>
               				 						</div>
               				 				</div>
               				 			</div>

               				 			<div class="iso-box wallpaper col-md-4 col-sm-6 col-xs-12">
               				 				<div class="portfolio-thumb">
               				 					<img src="images/audi5.jpg" class="fluid-img" alt=audi5.img">
               				 						<div class="portfolio-overlay">
               				 							<a href="#" class="fa fa-search"></a>
               				 							<a href="#" class="fa fa-link"></a>
               				 							<h4>Audi A5 67.000 Euro</h4>
               				 							<p>Shitet Audi A5, Viti 2018, Full Option, Euro 5</p>
               				 						</div>
               				 				</div>
               				 			</div>

               				 			<div class="iso-box graphic photoshop col-md-4 col-sm-6 col-xs-12">
               				 				<div class="portfolio-thumb">
               				 					<img src="images/a6.jpg" class="fluid-img" alt="a6 img">
               				 						<div class="portfolio-overlay">
               				 							<a href="#" class="fa fa-search"></a>
               				 							<a href="#" class="fa fa-link"></a>
               				 							<h4>Audi A3 20.000 Euro</h4>
               				 							<p>Audi A3,Viti 2017,Euro 5,Full Option</p>
               				 						</div>
               				 				</div>
               				 			</div>

               				 		</div>
               				 	</div>

							</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end portfolio -->

		<!-- start contact -->
		<div id="contact">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-4 wow fadeInLeft" data-wow-delay="0.6s">
						<h2><strong> Na </strong> Kontaktoni </h2>
						<p>Na kontaktoni ne faqet tona zyrtare per me shum informacion rreth asaj mbi te cilen jeni te interesuar.</p>
						<ul class="social-icon">
							<li><a href="#" class="fa fa-facebook"></a></li>
							<li><a href="#" class="fa fa-twitter"></a></li>
							<li><a href="#" class="fa fa-instagram"></a></li>
						</ul>
					</div>
					<div class="col-md-3 col-sm-4 wow fadeIn" data-wow-delay="0.9s">
						<address>
							<h3>Vizitoni zyrat tona!</h3>
							<p><i class="fa fa-map-marker too-icon"></i>Albania,Tirane 2001</p>
							<p><i class="fa fa-phone too-icon"></i>+355685541172</p>
							<p><i class="fa fa-envelope-o too-icon"></i>refik.osmani@umsh.edu.al</p>
															1					</address>
					</div>
					<form action="#" method="post" class="col-md-6 col-sm-4" id="contact-form" role="form">
							<div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
								<input name="name" type="text" class="form-control" id="name" placeholder="Name">
							</div>
							<div class="col-md-6 col-sm-12 wow fadeIn" data-wow-delay="0.3s">
								<input name="email" type="email" class="form-control" id="email" placeholder="Email">
							</div>
							<div class="col-md-12 col-sm-12 wow fadeIn" data-wow-delay="0.9s">
								<textarea name="message" rows="5" class="form-control" id="message" placeholder="Message"></textarea>
							</div>
							<div class="col-md-offset-9 col-md-3 col-sm-6 wow fadeIn" data-wow-delay="0.3s">
								<input name="submit" type="submit" class="form-control" id="submit" value="Send">
							</div>
					</form>
				</div>
			</div>
		</div>
		
				</div>
			</div>
		</footer>
		<!-- end footer -->


		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		<!-- bootstrap -->
		<script src="js/bootstrap.min.js"></script>
		<!-- isotope -->
		<script src="js/isotope.js"></script>
		<!-- images loaded -->
   		<script src="js/imagesloaded.min.js"></script>
   		<!-- wow -->
		<script src="js/wow.min.js"></script>
		<!-- smoothScroll -->
		<script src="js/smoothscroll.js"></script>
		<!-- jquery flexslider -->
		<script src="js/jquery.flexslider.js"></script>
		<!-- custom -->
		<script src="js/custom.js"></script>

	</body>
</html>
